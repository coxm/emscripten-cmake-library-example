#!/bin/bash
set -u
set -e

# Check if we have colours.
num_colours=$(tput colors)
if test -n "$num_colours" && test $num_colours -ge 8; then
  normal="$(tput sgr0)"
  header="$(tput bold)$(tput setaf 2)"
else
  normal=""
  header=""
fi

repo_root="$( dirname "$( dirname "$( readlink -f "${BASH_SOURCE[0]}" )" )" )"
echo "Repo root: $repo_root"

(
  build_name="native"
  build_dir="$repo_root/build/$build_name"
  echo "${header}Configuring $build_name build${normal}"
  set -x
  mkdir -p "$build_dir"
  cd "$build_dir"
  cmake "$repo_root"
  make
)

(
  build_name="emscripten"
  build_dir="$repo_root/build/$build_name"
  echo "${header}Configuring $build_name build${normal}"
  set -x
  mkdir -p "$build_dir"
  cd "$build_dir"
  emcmake cmake "$repo_root"
  emmake make
)
