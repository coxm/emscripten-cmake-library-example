#include <iostream>

#include "mylib/mylib.h"


int main() {
  std::cout << "2 + 2 = " << mylib::add(2, 2) << std::endl;
  return 0;
}
